# blockgap

Finds periods w/ scarse bitcoin block production

The program currently:
- Starts evaluating at block 391000 (start of 2016).
- Uses the median blocktime of the last three blocks.
- Counts blocks in the last 4 hours (expecting 24 blocks).
- Reports recent block counts of 6 or less (25% hash power).


## Running

```
export RPCUSER=rpcuser
export RPCPASSWORD=blahblahblah

./blockgap.py
```

This produces the following output:

```
height:514770 median:1521795123 date:2018-03-23 09:52:03 len(recent):6
height:622891 median:1585153689 date:2020-03-25 17:28:09 len(recent):6
height:688941 median:1624770341 date:2021-06-27 07:05:41 len(recent):6
height:688991 median:1624833061 date:2021-06-28 00:31:01 len(recent):6
height:688992 median:1624839190 date:2021-06-28 02:13:10 len(recent):4
height:688993 median:1624842223 date:2021-06-28 03:03:43 len(recent):4
height:688994 median:1624842994 date:2021-06-28 03:16:34 len(recent):5
height:688995 median:1624844484 date:2021-06-28 03:41:24 len(recent):6
```

This demonstrates that less than one such hash power fluctuation occurs per year.
